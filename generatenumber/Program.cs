using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using OpenFaaS.Hosting;
using StackExchange.Redis;
using System;
using System.IO;

var redisServer = Environment.GetEnvironmentVariable("REDIS_SERVER");
var expirySeconds = Convert.ToInt16(Environment.GetEnvironmentVariable("EXPIRY_SECONDS") ?? "300");

Runner.Run(args, builder =>
{
    // add your services to the container
    builder.Services.AddSingleton<ConnectionMultiplexer>(ConnectionMultiplexer.Connect(redisServer));
}, app =>
{
    // configure the HTTP request pipeline
    app.MapGet("/", (ConnectionMultiplexer redis) =>
    {
        IDatabase db = redis.GetDatabase();

        string[] telStarts = "133,149,153,173,177,180,181,189,199,130,131,132,145,155,156,166,171,175,176,185,186,166,134,135,136,137,138,139,147,150,151,152,157,158,159,172,178,182,183,184,187,188,198,170".Split(',');
        Random rnd = new Random();
        string number;
        do
        {
            int index = rnd.Next(0, telStarts.Length - 1);
            string first = telStarts[index];
            string second = (rnd.Next(100, 888) + 10000).ToString().Substring(1);
            string thrid = (rnd.Next(1, 9100) + 10000).ToString().Substring(1);
            number = first + second + thrid;
        } while (db.KeyExists(number));

        db.StringSet(number, "1234", expiry: TimeSpan.FromSeconds(expirySeconds));

        return new
        {
            number
        };
    });
});
