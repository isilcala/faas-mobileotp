using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using OpenFaaS.Hosting;
using StackExchange.Redis;
using System;
using System.IO;

var redisServer = Environment.GetEnvironmentVariable("REDIS_SERVER");

Runner.Run(args, builder =>
{
    // add your services to the container
    builder.Services.AddSingleton<ConnectionMultiplexer>(ConnectionMultiplexer.Connect(redisServer));
}, app =>
{
    // configure the HTTP request pipeline
    app.MapGet("/", (ConnectionMultiplexer redis) =>
    {
        IDatabase db = redis.GetDatabase();
        if (!db.KeyExists(""))
        {

        }
        db.StringSet("asdf", "asdf", expiry: TimeSpan.FromSeconds(60));
        return new
        {
            Message = redisServer + db.Ping(CommandFlags.None)
        };
    });
});
